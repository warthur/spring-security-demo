package com.warthur.spring.demo.common;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.io.Serializable;

/**
 * @author warthur
 * @date 2019/10/07
 */
@Data
public class Response<T> implements Serializable {
    private static final long serialVersionUID = 3298132694646370747L;

    private boolean success;
    private Integer code;
    private String message;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private T data;

    public Response(Integer code, String message) {
        this(code, message, null);
    }

    public Response(Integer code, String message, T data) {
        this.success = code == 0;
        this.code = code;
        this.message = message;
        this.data = data;
    }

    public Boolean getSuccess() {
        return code == 0;
    }

    public Boolean isSuccess() {
        return success;
    }
}
