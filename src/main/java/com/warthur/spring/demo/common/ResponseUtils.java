package com.warthur.spring.demo.common;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

/**
 * @author warthur
 * @date 2019/10/07
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class ResponseUtils {

    public static Response success(Object data) {
        return new Response<>(0, "请求成功", data);
    }

    public static Response error(int code, String message) {
        return new Response<>(code, message);
    }

    public static Response error(String message) {
        return error(1, message);
    }
}
