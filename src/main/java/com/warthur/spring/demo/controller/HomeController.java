package com.warthur.spring.demo.controller;

import com.warthur.spring.demo.common.JwtTokenUtils;
import com.warthur.spring.demo.common.Response;
import com.warthur.spring.demo.common.ResponseUtils;
import com.warthur.spring.demo.dao.UserDAO;
import com.warthur.spring.demo.pojo.domain.User;
import com.warthur.spring.demo.pojo.dto.UserDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author warthur
 * @date 2019/10/05
 */
@RestController
@Slf4j
public class HomeController {

    @Autowired
    private UserDAO userDAO;

    @GetMapping("/hello")
    public Response hello() {

        return ResponseUtils.success(new UserDTO(null, null));
    }

    @GetMapping("/test")
    public Response test() {

        throw new RuntimeException("test错误");
    }

    @PostMapping("/login")
    public Response login(String username, String password) {

        User user = userDAO.login(username);

        return ResponseUtils.success(JwtTokenUtils.token(user));
    }

    @PostMapping("/token/{openId}")
    public Response token(@PathVariable String openId) {

        User user = userDAO.token(openId);

        return ResponseUtils.success(JwtTokenUtils.token(user));
    }
}
