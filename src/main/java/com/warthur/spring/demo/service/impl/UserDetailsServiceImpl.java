package com.warthur.spring.demo.service.impl;

import com.warthur.spring.demo.dao.UserDAO;
import com.warthur.spring.demo.pojo.domain.User;
import com.warthur.spring.demo.service.UserDetailsService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

/**
 * @author warthur
 * @date 2019/10/05
 */
@Service
@Slf4j
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private UserDAO userDAO;

    @Override
    public User loadUserByUsername(String username) throws UsernameNotFoundException {
        return userDAO.login(username);
    }
}
