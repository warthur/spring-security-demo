package com.warthur.spring.demo.service;

import com.warthur.spring.demo.pojo.domain.User;

/**
 * @author warthur
 * @date 2019/10/06
 */
public interface UserDetailsService {

    User loadUserByUsername(String username);
}
