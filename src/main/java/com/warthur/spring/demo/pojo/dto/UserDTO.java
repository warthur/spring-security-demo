package com.warthur.spring.demo.pojo.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.warthur.spring.demo.pojo.BaseDTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * @author warthur
 * @date 2019/10/07
 */
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@Data
public class UserDTO extends BaseDTO {
    private static final long serialVersionUID = 7635330279574735359L;

    private String userName;
    private Integer age;
}
