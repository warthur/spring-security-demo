package com.warthur.spring.demo.pojo;

import lombok.Data;

import java.io.Serializable;

/**
 * @author warthur
 * @date 2019/10/07
 */
public class BaseDTO implements Serializable {
    private static final long serialVersionUID = -7650958643330297630L;
}
