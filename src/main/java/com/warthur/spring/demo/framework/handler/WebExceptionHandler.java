package com.warthur.spring.demo.framework.handler;

import com.warthur.spring.demo.common.Response;
import com.warthur.spring.demo.common.ResponseUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * @author warthur
 * @date 2019/10/05
 */
@RestControllerAdvice
@Slf4j
public class WebExceptionHandler {

    @ExceptionHandler({Exception.class})
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public Response serverExceptionGet(Exception e) {
        log.error("接口请求数据异常：", e);
        return ResponseUtils.error(1, e.getMessage());
    }
}
