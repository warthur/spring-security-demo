package com.warthur.spring.demo.framework.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.warthur.spring.demo.framework.serializer.JsonNullValueSerializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author warthur
 * @date 2019/10/07
 */
@Configuration
public class JacksonConverterConfig {

    @Bean
    public ObjectMapper objectMapper() {

        ObjectMapper objectMapper = new ObjectMapper();

        objectMapper.getSerializerProvider().setNullValueSerializer(new JsonNullValueSerializer());

        return objectMapper;
    }


}
