package com.warthur.spring.demo.dao;

import com.warthur.spring.demo.pojo.domain.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

/**
 * @author warthur
 * @date 2019/10/05
 */
@Mapper
public interface UserDAO {

    @Select("SELECT user_id, user_name, password, open_id FROM t_user WHERE user_name=#{userName}")
    User login(@Param("userName") String userName);

    @Select("SELECT user_id, user_name, password, open_id FROM t_user WHERE open_id=#{openId}")
    User token(@Param("openId") String openId);
}
